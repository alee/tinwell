/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "tinwell-internal.h"

static gboolean unregister_recorder(TinwellRecorder *object, GDBusMethodInvocation *invocation, const gchar *arg_app_name);
static gboolean register_recorder( TinwellRecorder *object, GDBusMethodInvocation *invocation, const gchar *arg_app_name);
static gboolean resume_recording( TinwellRecorder *object, GDBusMethodInvocation *invocation, const gchar *arg_app_name);
static gboolean pause_recording( TinwellRecorder *object, GDBusMethodInvocation *invocation, const gchar *arg_app_name);
static gboolean stop_recording( TinwellRecorder *object, GDBusMethodInvocation *invocation, const gchar *arg_app_name);
static gboolean start_recording( TinwellRecorder *object, GDBusMethodInvocation *invocation, const gchar *arg_app_name, const gchar *arg_file_path);
static void recording_error (GrassmoorAudioRecorder *recorder, const gchar *errorMsg, gpointer userData);
static void recording_completed (GrassmoorAudioRecorder *recorder, const gchar *recordFileLoc, gpointer userData);
static void recording_resumed (GrassmoorAudioRecorder *recorder, gpointer userData);
static void recording_paused (GrassmoorAudioRecorder *recorder, gpointer userData);
static void recording_started (GrassmoorAudioRecorder *recorder, gpointer userData);
static void audio_service_remove_recorder_app_from_hash(const gchar *appName);
static void audio_service_initialise_client_recorder_resources (TinwellRecData *clientAppData);
static void audio_service_add_recorder_client_to_hash(const gchar *appName, TinwellRecData *clientAppData);
static TinwellRecData* audio_service_create_recorder_client_struct(const gchar *appName);
static gboolean destroy_recorder_app_entry(gpointer key, gpointer value, gpointer userData);
static void audio_service_recorder_hash_deinit(void);
static void audio_service_recorder_hash_init(void);
static void audio_service_recorder_mutex_deinit(void);
static void audio_service_recorder_mutex_init(void);

static GHashTable *audioServiceRecorderHash = NULL;
static GMutex audioServiceRecorderMutex;
static TinwellRecorder *recorderObject = NULL;

static void audio_service_recorder_mutex_init(void)
{
	DEBUG ("entered");

	/* Allocate a new mutex. This mutex is used as a sync parameter to
	 * access the global application hash table. Although currently this
	 * may not be accessed through multiple threads, synchronisation
	 * mechanism is built in as design may change later */
	g_mutex_init(&audioServiceRecorderMutex);

}

static void audio_service_recorder_mutex_deinit(void)
{
	DEBUG ("entered");

	/* Free up the mutex */
	g_mutex_clear(&audioServiceRecorderMutex);

}

static void audio_service_recorder_hash_init(void)
{
	DEBUG ("entered");

	/* Allocate memory for the hash table. This hash table maintains
	 * a mapping of the application Id-> clientAppData(Recorder) for the application */
	audioServiceRecorderHash = g_hash_table_new( g_str_hash, g_str_equal );
}

static void audio_service_recorder_hash_deinit(void)
{
	DEBUG ("entered");

	/* Acquire the mutex lock before accessing the hash */
	g_mutex_lock(&audioServiceRecorderMutex);

	/* Free all memory held up for the app Struct in the hash table */
	g_hash_table_foreach_remove(audioServiceRecorderHash, (GHRFunc) destroy_recorder_app_entry, NULL);

	/* Destroy all memory allocated for the hash table */
	g_hash_table_destroy(audioServiceRecorderHash);

	/* Release the mutex lock */
	g_mutex_unlock(&audioServiceRecorderMutex);

	/* Set the hash table pointer to NULL */
	audioServiceRecorderHash = NULL;
}

static gboolean destroy_recorder_app_entry(gpointer key, gpointer value, gpointer userData)
{
	DEBUG ("entered");

	if(NULL != key && NULL != value)
	{
		TinwellRecData *recorderData = (TinwellRecData *) value;

		if(NULL != recorderData)
		{
			/* Free memory held for the member variables */
			if(NULL != recorderData->name)
			{
				g_free(recorderData->name);
				recorderData->name = NULL;
			}

			if(GRASSMOOR_IS_AUDIO_RECORDER(recorderData->recorder))
				g_object_unref(recorderData->recorder);

			g_free(recorderData);
			recorderData = NULL;
		}
	}

	return TRUE;
}

static TinwellRecData* audio_service_create_recorder_client_struct(const gchar *appName)
{
	DEBUG ("entered");

	TinwellRecData *clientAppData = g_new0(TinwellRecData, 1);

	/* Assign the parameters that are to be treated as default */
	if(NULL != clientAppData)
	{
		clientAppData = g_new0(TinwellRecData, 1);

		clientAppData->name = g_strdup(appName);
		clientAppData->recorder = NULL;
		clientAppData->recording = FALSE;
		clientAppData->audioMgrPause = FALSE;
		clientAppData->clientPause = FALSE;
	}

	return clientAppData;
}

static void audio_service_add_recorder_client_to_hash(const gchar *appName, TinwellRecData *clientAppData)
{
	DEBUG ("entered");

	if(NULL == appName || NULL == clientAppData)
		return;

	/* Acquire the mutex lock before accessing the hash */
	g_mutex_lock(&audioServiceRecorderMutex);

	/* Add the new entry into the hash table */
	gchar* appNameLocal = g_strdup(appName);
	g_hash_table_replace(audioServiceRecorderHash, (gpointer) appNameLocal, (gpointer) clientAppData);

	/* Release the mutex lock */
	g_mutex_unlock(&audioServiceRecorderMutex);
}

static void recording_started (GrassmoorAudioRecorder *recorder, gpointer userData)
{
	DEBUG ("entered");

	if( (GRASSMOOR_IS_AUDIO_RECORDER (recorder)) && (NULL != userData) )
	{
		TinwellRecorder *recorderObject = audio_service_recorder_object_get();
		if(GRASSMOOR_IS_AUDIO_RECORDER (recorderObject) )
		{
			TinwellRecData *recorderData = (TinwellRecData *)userData;

			/* Set both audio manager and client pause status to true */
			recorderData->audioMgrPause = FALSE;
			recorderData->clientPause = FALSE;

			tinwell_recorder_emit_recording_started(recorderObject, recorderData->name);
		}
	}
}

static void recording_paused (GrassmoorAudioRecorder *recorder, gpointer userData)
{
	DEBUG ("entered");

	if( (GRASSMOOR_IS_AUDIO_RECORDER (recorder)) && (NULL != userData) )
	{
		TinwellRecorder *recorderObject = audio_service_recorder_object_get();
		if(GRASSMOOR_IS_AUDIO_RECORDER (recorderObject) )
		{
			TinwellRecData *recorderData = (TinwellRecData *)userData;
			tinwell_recorder_emit_recording_paused(recorderObject, recorderData->name);
		}
	}
}

static void recording_resumed (GrassmoorAudioRecorder *recorder, gpointer userData)
{
	DEBUG ("entered");

	if( (GRASSMOOR_IS_AUDIO_RECORDER (recorder)) && (NULL != userData) )
	{
		TinwellRecorder *recorderObject = audio_service_recorder_object_get();
		if(GRASSMOOR_IS_AUDIO_RECORDER (recorderObject) )
		{
			TinwellRecData *recorderData = (TinwellRecData *)userData;

			/* Set both audio manager and client pause status to true */
			recorderData->audioMgrPause = FALSE;
			recorderData->clientPause = FALSE;

			tinwell_recorder_emit_recording_resumed(recorderObject, recorderData->name);
		}
	}
}

static void recording_completed (GrassmoorAudioRecorder *recorder, const gchar *recordFileLoc, gpointer userData)
{
	DEBUG ("entered");

	if( (GRASSMOOR_IS_AUDIO_RECORDER (recorder)) && (NULL != userData) )
	{
		CanterburyAudioMgr *proxyAudioMgr = audio_service_get_audio_mgr_proxy();
		if(CANTERBURY_IS_AUDIO_MGR (proxyAudioMgr) )
		{
			TinwellRecData *recorderData = (TinwellRecData *)userData;

			/* If recording mode was exclusive, send a request to
			 * release the audio resource */
			if(CANTERBURY_AUDIO_RECORD_TYPE_EXCLUSIVE == recorderData->recorderType)
				canterbury_audio_mgr_call_audio_release(proxyAudioMgr, recorderData->name, NULL, NULL, NULL);	

			tinwell_recorder_emit_recording_completed(recorderObject, recorderData->name, recordFileLoc);
		}
	}
}

static void recording_error (GrassmoorAudioRecorder *recorder, const gchar *errorMsg, gpointer userData)
{
	DEBUG ("entered");

	if( (GRASSMOOR_IS_AUDIO_RECORDER (recorder)) && (NULL != userData) )
	{
		TinwellRecorder *recorderObject = audio_service_recorder_object_get();
		if(GRASSMOOR_IS_AUDIO_RECORDER (recorderObject) )
		{
			TinwellRecData *recorderData = (TinwellRecData *)userData;
			tinwell_recorder_emit_recording_error(recorderObject, recorderData->name, errorMsg);
		}
	}
}

static void audio_service_initialise_client_recorder_resources (TinwellRecData *clientAppData)
{
	DEBUG ("entered");
	if(NULL != clientAppData)
	{
		clientAppData->recorder = grassmoor_audio_recorder_new(); 

		g_signal_connect(clientAppData->recorder, "recorder-started", G_CALLBACK(recording_started), clientAppData);
		g_signal_connect(clientAppData->recorder, "recorder-paused", G_CALLBACK(recording_paused), clientAppData);
		g_signal_connect(clientAppData->recorder, "recorder-resumed", G_CALLBACK(recording_resumed), clientAppData);
		g_signal_connect(clientAppData->recorder, "recorder-completed", G_CALLBACK(recording_completed), clientAppData);
		g_signal_connect(clientAppData->recorder, "recorder-error", G_CALLBACK(recording_error), clientAppData);
	}
}

static void audio_service_remove_recorder_app_from_hash(const gchar *appName)
{
	DEBUG ("entered");

	if(NULL != appName)
	{
		TinwellRecData *clientAppData = g_hash_table_lookup(audioServiceRecorderHash, (gpointer) appName);

		if(NULL != clientAppData)
		{	
			/* destroy the application specific book keeping params held
			 * by audio service*/
			destroy_recorder_app_entry( (gpointer)appName, clientAppData, NULL);

			/* destroy application entry in audio service hash */
			g_mutex_lock(&audioServiceRecorderMutex);
			gboolean remove = g_hash_table_remove(audioServiceRecorderHash, (gpointer)appName);
			DEBUG ("Recorder remove state = %d ", remove);
			g_mutex_unlock(&audioServiceRecorderMutex);
		}
	}
}

static gboolean start_recording( TinwellRecorder *object, GDBusMethodInvocation *invocation, const gchar *arg_app_name, const gchar *arg_file_path)
{
	if ( (NULL != arg_app_name) )
	{
		DEBUG ("Start recording request by %s", arg_app_name);

		TinwellRecData *recorderData = audio_service_get_recorder_client_struct(arg_app_name);

		if( (NULL != recorderData) && (GRASSMOOR_IS_AUDIO_RECORDER(recorderData->recorder)) )
		{
			g_object_set(recorderData->recorder, "record-file-location", arg_file_path, NULL);
			recorderData->clientPause = FALSE;

			if( CANTERBURY_AUDIO_RECORD_TYPE_MIXED == recorderData->recorderType )
			{
				DEBUG ("Recorder type is MIXED. Start recording for%s ", arg_app_name);

				g_object_set(recorderData->recorder, "recording", TRUE, NULL);
			}
			else
			{
				DEBUG ("Recorder type is Exclusive. Send audio request %s", arg_app_name);

				canterbury_audio_mgr_call_audio_request(audio_service_get_audio_mgr_proxy(), arg_app_name, NULL, NULL, NULL);  
			}
		}
		else
		{
			DEBUG ("Error msg = Cannot register client");

			g_dbus_method_invocation_return_dbus_error (invocation, "Tinwell.Recorder.Error.Failed", "Client not registered");
			return TRUE;
		}
	}
	else
	{
		DEBUG ("Error msg = Invalid App Name");

		g_dbus_method_invocation_return_dbus_error (invocation, "Tinwell.Recorder.Error.Failed", "Invalid app name");
		return TRUE;
	}

	tinwell_recorder_complete_start_recording(object, invocation);

	return TRUE;
}

static gboolean stop_recording( TinwellRecorder *object, GDBusMethodInvocation *invocation, const gchar *arg_app_name)
{
	if ( (NULL != arg_app_name) )
	{
		DEBUG ("Stop recording request by %s", arg_app_name);

		TinwellRecData *recorderData = audio_service_get_recorder_client_struct(arg_app_name);

		if( (NULL != recorderData) && (GRASSMOOR_IS_AUDIO_RECORDER(recorderData->recorder)) )
		{
			if(CANTERBURY_AUDIO_RECORD_TYPE_EXCLUSIVE == recorderData->recorderType)
			{
				DEBUG ("Recorder type is Exclusive. Send audio release for %s", arg_app_name);

				/* Release audio resource for exclusive recordings */
				CanterburyAudioMgr *proxyAudioMgr = audio_service_get_audio_mgr_proxy();
				
				if( (CANTERBURY_IS_AUDIO_MGR (proxyAudioMgr)) )
					canterbury_audio_mgr_call_audio_release(proxyAudioMgr, recorderData->name, NULL, NULL, NULL);	
			}
			
			DEBUG ("Stop recording for %s", arg_app_name);

			/* TO DO: Check when stop needs to be called, (maybe
			 * later we need to wait for message from audio manager
			 * and then stop) */
			grassmoor_audio_recorder_stop_recording(recorderData->recorder);
		}
		else
		{
			DEBUG ("Error msg = Cannot register client");

			g_dbus_method_invocation_return_dbus_error (invocation, "Tinwell.Recorder.Error.Failed", "Client not registered");
			return TRUE;
		}
	}
	else
	{
		DEBUG ("Error msg = Invalid App Name");

		g_dbus_method_invocation_return_dbus_error (invocation, "Tinwell.Recorder.Error.Failed", "Invalid app name");
		return TRUE;
	}

	tinwell_recorder_complete_stop_recording(object, invocation);

	return TRUE;
}

static gboolean pause_recording( TinwellRecorder *object, GDBusMethodInvocation *invocation, const gchar *arg_app_name)
{
	if ( (NULL != arg_app_name) )
	{
		DEBUG ("Pause recording request by %s", arg_app_name);

		TinwellRecData *recorderData = audio_service_get_recorder_client_struct(arg_app_name);

		if( (NULL != recorderData) && (GRASSMOOR_IS_AUDIO_RECORDER(recorderData->recorder)) )
		{
			DEBUG ("Recording for %s to be paused", arg_app_name);

			recorderData->clientPause = TRUE;

			g_object_set(recorderData->recorder, "recording", FALSE, NULL);
		}
		else
		{
			DEBUG ("Error msg = Client not registered");

			g_dbus_method_invocation_return_dbus_error (invocation, "Tinwell.Recorder.Error.Failed", "Client not registered");
			return TRUE;
		}
	}
	else
	{
		DEBUG ("Error msg = Invalid App Name");

		g_dbus_method_invocation_return_dbus_error (invocation, "Tinwell.Recorder.Error.Failed", "Invalid app name");
		return TRUE;
	}

	tinwell_recorder_complete_pause_recording(object, invocation);

	return TRUE;
}

static gboolean resume_recording( TinwellRecorder *object, GDBusMethodInvocation *invocation, const gchar *arg_app_name)
{
	if ( (NULL != arg_app_name) )
	{
		DEBUG ("Pause recording request by %s", arg_app_name);

		TinwellRecData *recorderData = audio_service_get_recorder_client_struct(arg_app_name);

		if( (NULL != recorderData) && (GRASSMOOR_IS_AUDIO_RECORDER(recorderData->recorder)) )
		{
			recorderData->clientPause = FALSE;

			if( CANTERBURY_AUDIO_RECORD_TYPE_MIXED == recorderData->recorderType )
			{
				DEBUG ("Recorder type is MIXED. Start recording for %s", arg_app_name);

				g_object_set(recorderData->recorder, "recording", TRUE, NULL);
			}
			else
			{
				DEBUG ("Recorder type is EXCLUSIVE. Make audio request for %s", arg_app_name);

				canterbury_audio_mgr_call_audio_request(audio_service_get_audio_mgr_proxy(), arg_app_name, NULL, NULL, NULL);  
			}
		}
		else
		{
			DEBUG ("Error msg = Client not registered");

			g_dbus_method_invocation_return_dbus_error (invocation, "Tinwell.Recorder.Error.Failed", "Client not registered");
			return TRUE;
		}
	}
	else
	{
		DEBUG ("Error msg = Invalid App Name");

		g_dbus_method_invocation_return_dbus_error (invocation, "Tinwell.Recorder.Error.Failed", "Invalid app name");
		return TRUE;
	}

	tinwell_recorder_complete_resume_recording(object, invocation);

	return TRUE;
}

static gboolean register_recorder( TinwellRecorder *object, GDBusMethodInvocation *invocation, const gchar *arg_app_name)
{
	if(NULL != arg_app_name)
	{
		DEBUG ("Register client %s", arg_app_name);

		/* Check if the client is already registered. If so just return.
		 * If not, register the client */
		TinwellRecData *recorderData = audio_service_get_recorder_client_struct(arg_app_name);
		if(NULL != recorderData)
		{
			/* Already registered. No duplicate registrations */
			tinwell_recorder_complete_register_recorder(object, invocation, TRUE);
		}
		else
		{
			recorderData = audio_service_create_recorder_client_struct(arg_app_name);
			if(NULL != recorderData)
			{
				/* Add new client details to service hash list */
				audio_service_add_recorder_client_to_hash(arg_app_name, recorderData);

				/* Initialise recorder specific resources */
				audio_service_initialise_client_recorder_resources(recorderData);

				/* Registration Success */
				tinwell_recorder_complete_register_recorder(object, invocation, TRUE);
			}
			else
			{
				DEBUG ("Error msg = Cannot reg client");

				g_dbus_method_invocation_return_dbus_error (invocation, "Tinwell.Recorder.Error.Failed", "Cannot register client");

				/* Registration Failure */
				tinwell_recorder_complete_register_recorder(object, invocation, FALSE);
			}

		}
	}
	else
	{
		DEBUG ("Error msg = Invalid App Name");

		g_dbus_method_invocation_return_dbus_error (invocation, "Tinwell.Recorder.Error.Failed", "Invalid app name");

		/* Registration Failure */
		tinwell_recorder_complete_register_recorder(object, invocation, FALSE);
	}
	return TRUE;
}

static gboolean unregister_recorder(TinwellRecorder *object, GDBusMethodInvocation *invocation, const gchar *arg_app_name)
{
	if(NULL != arg_app_name)
	{
		DEBUG ("UnRegister client %s", arg_app_name);

		/* Fetch the app struct from the hash. If there is no
		 * entry in the hash, signal an error message */
		TinwellRecData *recorderData = audio_service_get_recorder_client_struct ( (gchar *)arg_app_name);

		if(NULL != recorderData)
		{
			/* Remove application entry from audio service hash tables
			 * */
			audio_service_remove_recorder_app_from_hash(arg_app_name);
		
			DEBUG ("Remove app %s from hash", arg_app_name);

			/* Unregistration successful */
			tinwell_recorder_complete_unregister_recorder(object, invocation, TRUE);
		}
		else
		{
			DEBUG ("Error msg = Client not registered");

			g_dbus_method_invocation_return_dbus_error (invocation, "Tinwell.Recorder.Error.Failed", "Client Not Registerd!");

			/* Unregistration not successful */
			tinwell_recorder_complete_unregister_recorder(object, invocation, FALSE);
		}
	}
	else
	{
		DEBUG ("Error msg = Invalid App Name");

		g_dbus_method_invocation_return_dbus_error (invocation, "Tinwell.Recorder.Error.Failed", "Client Not Registerd!");

		/* Unregistration not successful */
		tinwell_recorder_complete_unregister_recorder(object, invocation, FALSE);
	}

	return TRUE;
}

static void audio_service_register_recorder_handlers(TinwellRecorder *recorderObject)
{
	DEBUG ("entered");

	g_signal_connect (recorderObject, "handle-register-recorder", G_CALLBACK (register_recorder), NULL);
	g_signal_connect (recorderObject, "handle-unregister-recorder", G_CALLBACK (unregister_recorder), NULL);
	g_signal_connect (recorderObject, "handle-start-recording", G_CALLBACK (start_recording), NULL);
	g_signal_connect (recorderObject, "handle-stop-recording", G_CALLBACK (stop_recording), NULL);
	g_signal_connect (recorderObject, "handle-pause-recording", G_CALLBACK (pause_recording), NULL);
	g_signal_connect (recorderObject, "handle-resume-recording", G_CALLBACK (resume_recording), NULL);
}

TinwellRecData* audio_service_get_recorder_client_struct(const gchar *appName)
{
	DEBUG ("entered");

	TinwellRecData *clientAppData = NULL;

	//if(NULL != audioServiceRecorderMutex && NULL != audioServiceRecorderMutex)
	//if(NULL != audioServiceRecorderMutex)
	{
		/* Acquire the mutex lock before accessing the hash */
		g_mutex_lock(&audioServiceRecorderMutex);

		/* Add the new entry into the hash table */
		clientAppData = g_hash_table_lookup(audioServiceRecorderHash, (gpointer) appName);

		/* Release the mutex lock */
		g_mutex_unlock(&audioServiceRecorderMutex);
	}

	return clientAppData;
}

void audio_service_recorder_resources_deinitialise(void)
{
	DEBUG ("entered");

	/* Deallocate all resources held for the hash table */
	audio_service_recorder_hash_deinit();

	/* Deallocate the resources needed for the mutex */
	audio_service_recorder_mutex_deinit();
}

void audio_service_initialise_recorder_resources(void)
{
	DEBUG ("entered");


		audio_service_recorder_mutex_init();

	if( NULL == audioServiceRecorderHash )
		audio_service_recorder_hash_init();
}

void audio_service_recorder_interface_initialize(GDBusConnection *connection)
{
	DEBUG ("entered");

	/* Creates a new skeleton object, ready to be exported */
        recorderObject = tinwell_recorder_skeleton_new();

        /* provide signal handlers for all methods */
        audio_service_register_recorder_handlers(recorderObject);

        /* Exports interface recorderObject at object_path "/Tinwell/Recorder" on connection */
        if (!g_dbus_interface_skeleton_export (G_DBUS_INTERFACE_SKELETON (recorderObject), connection, "/org/apertis/Tinwell/Recorder", NULL))
	          CRITICAL ("recorder interface skeleton export error");

}

TinwellRecorder* audio_service_recorder_object_get(void)
{
	return recorderObject;
}
