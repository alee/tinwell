/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <glib.h>
#include <gio/gio.h>
#include <glib/gstdio.h>
#include <stdio.h>
#include <stdlib.h>
#include "tinwell-internal.h"
#include "tinwell.h"
#include <clutter-gst/clutter-gst.h>
#include <sys/prctl.h>

#define INVALID_AUDIO_RESOURCE "App does not have a valid audio resource to play audio!"
static void audio_service_cleanup(void );
static void register_exit_functionalities(void);
static void termination_handler (int signum);
static void audio_service_canterbury_name_vanished(GDBusConnection *connection, const gchar *name, gpointer user_data);
static void audio_service_canterbury_name_appeared (GDBusConnection *connection, const gchar *name, const gchar *name_owner, gpointer user_data);

static guint playerOwnerID;

static void termination_handler (int signum)
{
	exit(0);
}

static void register_exit_functionalities(void)
{
	struct sigaction action;
	action.sa_handler = termination_handler;
	action.sa_flags = 0;
	sigaction(SIGINT, &action, NULL);
	sigaction(SIGHUP, &action, NULL);
	sigaction(SIGTERM, &action, NULL);
	atexit(audio_service_cleanup);
}

static void audio_service_cleanup(void )
{
	DEBUG ("entered");

	/*Deinitialise all resources allocated for the audio service player
	 * and recorder interface*/

	audio_service_plyr_resources_deinitialise();

	audio_service_recorder_resources_deinitialise();
}

static void audio_service_canterbury_name_vanished(GDBusConnection *connection, const gchar *name, gpointer user_data)
{
	DEBUG ("entered");

	update_on_audio_mgr_vanished();
	WARNING ("Canterbury is not available, waiting for it");
}

static void audio_service_canterbury_name_appeared (GDBusConnection *connection, const gchar *name, const gchar *name_owner, gpointer user_data)
{
	DEBUG ("name= %s, name-owner = %s", name, name_owner);

	 canterbury_audio_mgr_proxy_new (
                        connection,
                        G_DBUS_PROXY_FLAGS_NONE,
                        "org.apertis.Canterbury",
                        "/org/apertis/Canterbury/AudioMgr",
                        NULL,
                        audio_manager_proxy_cb,
                        NULL);

	update_on_audio_mgr_appeared();
}

void audio_service_resources_initialise(void)
{
	DEBUG ("Get the player and the recorder interface bus ..");

	register_exit_functionalities();

	audio_service_initialise_player_resources();

	audio_service_initialise_recorder_resources();

	playerOwnerID = g_bus_own_name (	G_BUS_TYPE_SESSION,           // bus type
										"org.apertis.Tinwell",  // interface name
										G_BUS_NAME_OWNER_FLAGS_NONE,  // bus own flag, can be used to take away the bus and give it to another service
										audio_service_bus_acquired,              // callback invoked when the bus is acquired
										audio_service_name_acquired,             // callback invoked when interface name is acquired
										audio_service_name_lost,                 // callback invoked when name is lost to another service or other reason
										NULL,                         // user data
										NULL);                        // user data free func


}

gint main(gint argc, gchar *argv[])
{
	prctl (PR_SET_PDEATHSIG, SIGKILL);
	gst_init(&argc, &argv);

	clutter_gst_init(&argc, & argv);

	/* Do a watch on the canterbury.service interface. This is to extract app specific info
	   before play is triggered for apps */
	g_bus_watch_name (	G_BUS_TYPE_SESSION,
			"org.apertis.Canterbury",
			G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
			audio_service_canterbury_name_appeared,
			audio_service_canterbury_name_vanished,
			NULL,
			NULL);

	GMainLoop *loop = g_main_loop_new(NULL, FALSE);
	g_main_loop_run(loop);

	g_bus_unown_name(playerOwnerID);

	exit (0);
}

