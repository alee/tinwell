/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "tinwell-internal.h"
#include <stdlib.h>

static void audio_response_cb (CanterburyAudioMgr *object, const gchar *arg_app_name, guint arg_audio_result);

static CanterburyAudioMgr *proxyAudioMgr = NULL;

// To exit if started before Audio manager (canterbury)
static gboolean audioMgrBusAppeared = FALSE;

static void audio_response_cb (CanterburyAudioMgr *object, const gchar *arg_app_name, guint arg_audio_result)
{
	DEBUG ("entered");

	if(NULL != arg_app_name)
	{
		DEBUG ("App Name = %s Audio Response = %d", arg_app_name, arg_audio_result);

		TinwellPlyrData *plyrData = audio_service_get_plyr_client_struct(arg_app_name);
		TinwellRecData *recorderData = audio_service_get_recorder_client_struct(arg_app_name);

		switch(arg_audio_result)
		{
			case CANTERBURY_AUDIO_PLAY:
				{
					if( (NULL != plyrData) && (GRASSMOOR_IS_AV_PLAYER(plyrData->player)) )
					{
						DEBUG ("Audio Resource approved for %s. Play can be triggered", arg_app_name);

						plyrData->audioMgrPause = FALSE;

						/* Start playback only if user has not requested
						 * for a pause */
						if(FALSE == plyrData->clientPause)
							g_object_set(plyrData->player, "playing", TRUE, NULL);
					}
					else
					{
						if( (NULL != recorderData) && (GRASSMOOR_IS_AUDIO_RECORDER(recorderData->recorder)) )
						{
					DEBUG ("Audio Resource approved for %s. Recording can be triggered", arg_app_name);

							recorderData->audioMgrPause = FALSE;

							/* Start recording only if user has not
							 * requested for a pause */
							if(FALSE == recorderData->clientPause)
								g_object_set(recorderData->recorder, "recording", TRUE, NULL);
						}
					}
					break;
				}

			case CANTERBURY_AUDIO_STOP:
				{
				  if( (NULL != plyrData) && (GRASSMOOR_IS_AV_PLAYER(plyrData->player)) )
					{
					  /* Cleanup audio channel for app in case a stop is received */
					  if( ( ( plyrData->audioResourceType == CANTERBURY_AUDIO_RESOURCE_INTERRUPT ) ||
                  ( plyrData->audioResourceType == CANTERBURY_AUDIO_RESOURCE_PHONE) )
                 &&
                ( GRASSMOOR_IS_AV_PLAYER(plyrData->player) ) )
            {
             g_object_unref(plyrData->player);
            }
					//  audio_service_remove_player_app_from_hash(arg_app_name);
          }
				}
				break;
			case CANTERBURY_AUDIO_PAUSE:
				{
					if( (NULL != plyrData) && (GRASSMOOR_IS_AV_PLAYER(plyrData->player)) )
					{
						DEBUG ("Audio Resource approved for %s. Play can be triggered", arg_app_name);

						plyrData->audioMgrPause = TRUE;
						g_object_set(plyrData->player, "playing", FALSE, NULL);
					}
					else
					{
						if( (NULL != recorderData) && (GRASSMOOR_IS_AUDIO_RECORDER (recorderData->recorder)))
						{
							DEBUG ("Audio Resource approved for %s. Recording can be triggered", arg_app_name);

							recorderData->audioMgrPause = TRUE;
							g_object_set(recorderData->recorder, "recording", FALSE, NULL);
						}
					}
					break;
				}

				break;

			default:
				break;
		}
	}
}

void audio_manager_proxy_cb( GObject *source_object, GAsyncResult *res, gpointer user_data)
{
	DEBUG ("entered");

	GError *error;

	/* finishes the proxy creation and gets the proxy ptr */
	proxyAudioMgr = canterbury_audio_mgr_proxy_new_finish (res , &error);

	if(NULL == proxyAudioMgr)
	{
		WARNING("Audio Manager is not available.");
	}
	else
	{
		g_signal_connect(proxyAudioMgr, "audio-response", G_CALLBACK (audio_response_cb), NULL);

		audio_service_resources_initialise();
	}
}

void update_on_audio_mgr_vanished()
{
	DEBUG ("entered");

	if(NULL != proxyAudioMgr)
		g_object_unref(proxyAudioMgr);

	g_warning("Audio Manager is not available \n");

}

void update_on_audio_mgr_appeared ()
{
	DEBUG ("entered");

	audioMgrBusAppeared = TRUE;
}

CanterburyAudioMgr* audio_service_get_audio_mgr_proxy(void)
{
	return proxyAudioMgr;
}
