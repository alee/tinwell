/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __TINWELL_H__
#define __TINWELL_H__

#include <glib.h>
#include <gio/gio.h>
#include "tinwell.h"
#include "libgrassmoor.h"
#include <canterbury/gdbus/canterbury.h>

#define DEBUG(format, ...) g_debug ("%s: " format, G_STRFUNC, ##__VA_ARGS__)
#define CRITICAL(format, ...) g_critical ("%s: " format, G_STRFUNC, ##__VA_ARGS__)
#define WARNING(format, ...) g_warning ("%s: " format, G_STRFUNC, ##__VA_ARGS__)

typedef struct _TinwellPlyrData TinwellPlyrData;;
typedef struct _TinwellRecData TinwellRecData;;
typedef struct _TinwellAudSrvRequest TinwellAudSrvRequest;

struct _TinwellPlyrData
{
	gboolean playState;
	gboolean repeat;
	gboolean shuffle;
	gboolean clientPause;
	gboolean audioMgrPause;
	gint playIndex;
	gchar *name;
	GrassmoorAVPlayer *player;
	GList *songsList;
	gchar *curPlayingTrack;
	guint audioResourceType;
};

struct _TinwellRecData
{
	gboolean recording;
	gboolean audioMgrPause;
	gboolean clientPause;
	CanterburyAudioRecordType recorderType;
	gchar *name;
	GrassmoorAudioRecorder *recorder;
};

void audio_service_resources_initialise(void);
CanterburyAppManager* tw_get_app_mgr_service_proxy(void);
void audio_service_get_app_info_from_app_mgr(GObject *appMgrObject, GAsyncResult *res, gpointer userData);

/* Tinwell Player */
void audio_service_initialise_player_resources(void);
void audio_service_plyr_resources_deinitialise(void);
TinwellPlayer* audio_service_player_object_get(void);
TinwellPlyrData* audio_service_get_plyr_client_struct(const gchar *appName);
gchar* audio_service_get_app_to_play(void);
void audio_service_indicate_media_error (ClutterMedia *media , GError *error, TinwellPlyrData *plyrData);
void audio_service_remove_player_app_from_hash(const gchar *appName);

/* Tinwell Recorder */
void audio_service_recorder_interface_initialize(GDBusConnection *connection);
void audio_service_initialise_recorder_resources(void);
void audio_service_recorder_resources_deinitialise(void);
TinwellRecorder* audio_service_recorder_object_get(void);
TinwellRecData* audio_service_get_recorder_client_struct(const gchar *appName);

/* Audio Manager interface */
void update_on_audio_mgr_appeared ();
void update_on_audio_mgr_vanished ();
void audio_manager_proxy_cb( GObject *source_object, GAsyncResult *res, gpointer user_data);
TinwellPlayer* audio_service_player_object_get(void);
void audio_service_bus_acquired (GDBusConnection *connection, const gchar *name, gpointer user_data);
gchar* audio_service_get_app_to_play(void);
void audio_service_name_acquired (GDBusConnection *connection, const gchar *name, gpointer user_data);
void audio_service_name_lost (GDBusConnection *connection, const gchar *name, gpointer user_data);
void audio_service_audio_mgr_name_appeared (GDBusConnection *connection, const gchar *name, const gchar *name_owner, gpointer user_data);
CanterburyAudioMgr* audio_service_get_audio_mgr_proxy(void);
void audio_service_audio_mgr_name_vanished(GDBusConnection *connection, const gchar *name, gpointer user_data);
TinwellPlyrData* audio_service_get_plyr_client_struct(const gchar *appName);
TinwellRecorder* audio_service_recorder_object_get(void);
TinwellRecData* audio_service_get_recorder_client_struct(const gchar *appName);

#endif
