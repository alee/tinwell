/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "tinwell-internal.h"

static void audio_service_plyr_mutex_init(void);
static void audio_service_plyr_mutex_deinit(void);
static void audio_service_plyr_hash_init(void);
static void audio_service_plyr_hash_deinit(void);
static void audio_service_add_plyr_client_to_hash(const gchar *appName, TinwellPlyrData *clientAppData);
static gboolean destroy_plyr_app_entry(gpointer key, gpointer value, gpointer userData);
static void audio_service_initialise_client_player_resources (TinwellPlyrData *clientAppData);
static TinwellPlyrData* audio_service_create_plyr_client_struct(const gchar *appName);
static void update_play_status (ClutterMedia *media ,GParamSpec *args1, TinwellPlyrData *plyrData);
static void update_play_progress(ClutterMedia *media , GParamSpec *psepc, TinwellPlyrData *plyrData);
static void notify_end_of_playback (ClutterMedia *media,  TinwellPlyrData *plyrData);
static void update_buffering_percentage (ClutterMedia *media ,GParamSpec *args1, TinwellPlyrData *plyrData);
//static void update_download_buffering(ClutterActor *player,gdouble start, gdouble stop, TinwellPlyrData *plyrData);
static void audio_service_free_player_tracks_list (TinwellPlyrData *clientAppData);
static void register_player_callbacks(TinwellPlyrData *plyrData);
static gboolean start_playback ( TinwellPlayer *object, GDBusMethodInvocation *invocation, const gchar *arg_app_name, const gchar *arg_url_path, gint arg_index);
static gboolean pause_playback ( TinwellPlayer *object, GDBusMethodInvocation *invocation, const gchar *arg_app_name);
static gboolean resume_playback ( TinwellPlayer *object, GDBusMethodInvocation *invocation, const gchar *arg_app_name);
static gboolean seek_track ( TinwellPlayer *object, GDBusMethodInvocation *invocation, const gchar *arg_app_name, gdouble seekPos);
static gboolean buffer_track (TinwellPlayer *object, GDBusMethodInvocation *invocation, const gchar *arg_app_name, gint arg_buffer_size);
static gboolean set_repeat_state (TinwellPlayer *object, GDBusMethodInvocation *invocation, const gchar *arg_app_name, gboolean repeatState);
static gboolean set_shuffle_state (TinwellPlayer *object, GDBusMethodInvocation *invocation, const gchar *arg_app_name, gboolean shuffleState);
static gboolean share_tracks (TinwellPlayer *object, GDBusMethodInvocation *invocation, const gchar *arg_app_name, const gchar *const *arg_tracks_list, gboolean new_list);
static gboolean unregister_player(TinwellPlayer *object, GDBusMethodInvocation *invocation, const gchar *arg_app_name);
static gboolean register_player(TinwellPlayer *object, GDBusMethodInvocation *invocation, const gchar *arg_app_name);
static void register_player_handlers(TinwellPlayer *playerObject);
static void trigger_playback (TinwellPlyrData *plyrData);
static void indicate_media_error (ClutterMedia *media , GError *error, TinwellPlyrData *plyrData);

static GHashTable *audioServicePlyrHash = NULL;
static GMutex audioServicePlyrMutex;
static TinwellPlayer *playerObject = NULL;
static gchar *appToPlay = NULL;

static void audio_service_plyr_mutex_init(void)
{
	DEBUG ("entered");

	/* Allocate a new mutex. This mutex is used as a sync parameter to
	 * access the global application hash table. Although currently this
	 * may not be accessed through multiple threads, synchronisation
	 * mechanism is built in as design may change later */

	g_mutex_init(&audioServicePlyrMutex);
}

static void audio_service_plyr_mutex_deinit(void)
{
	DEBUG ("entered");

	/* Free up the mutex */
	g_mutex_clear(&audioServicePlyrMutex);

}

static void audio_service_plyr_hash_init(void)
{
	DEBUG ("entered");

	/* Allocate memory for the hash table. This hash table maintains
	 * a mapping of the application Id-> clientAppData for the application */
	audioServicePlyrHash = g_hash_table_new( g_str_hash, g_str_equal );
}

static void audio_service_plyr_hash_deinit(void)
{
	DEBUG ("entered");

	/* Acquire the mutex lock before accessing the hash */
	g_mutex_lock(&audioServicePlyrMutex);

	/* Free all memory held up for the app Struct in the hash table */
	g_hash_table_foreach_remove(audioServicePlyrHash, (GHRFunc) destroy_plyr_app_entry, NULL);

	/* Destroy all memory allocated for the hash table */
	g_hash_table_destroy(audioServicePlyrHash);

	/* Release the mutex lock */
	g_mutex_unlock(&audioServicePlyrMutex);

	/* Set the hash table pointer to NULL */
	audioServicePlyrHash = NULL;
}

static void audio_service_add_plyr_client_to_hash(const gchar *appName, TinwellPlyrData *clientAppData)
{
	DEBUG ("entered");

	if(NULL == appName || NULL == clientAppData)
		return;

	/* Acquire the mutex lock before accessing the hash */
	g_mutex_lock(&audioServicePlyrMutex);

	/* Add the new entry into the hash table */
	gchar* appNameLocal = g_strdup(appName);
	g_hash_table_replace(audioServicePlyrHash, (gpointer) appNameLocal, (gpointer) clientAppData);

	/* Release the mutex lock */
	g_mutex_unlock(&audioServicePlyrMutex);
}

static gboolean destroy_plyr_app_entry(gpointer key, gpointer value, gpointer userData)
{
	DEBUG ("entered");

	if(NULL != key && NULL != value)
	{
		TinwellPlyrData *plyrData = (TinwellPlyrData *) value;

		if(NULL != plyrData)
		{
			/* Free memory held for the member variables */
			if(NULL != plyrData->name)
			{
				g_free(plyrData->name);
				plyrData->name = NULL;
			}
			if(NULL != plyrData->curPlayingTrack);
			{
				g_free(plyrData->curPlayingTrack);
				plyrData->curPlayingTrack = NULL;
			}

			/* Free up all memory held for the list of tracks */
			audio_service_free_player_tracks_list(plyrData);

			if(GRASSMOOR_IS_AV_PLAYER(plyrData->player))
				g_object_unref(plyrData->player);

			g_free(plyrData);
			plyrData = NULL;
		}
	}

	return TRUE;
}

static void register_player_handlers(TinwellPlayer *playerObject)
{
	g_signal_connect (playerObject, "handle-register-player", G_CALLBACK (register_player), NULL);
	g_signal_connect (playerObject, "handle-unregister-player", G_CALLBACK (unregister_player), NULL);
	g_signal_connect (playerObject, "handle-start-playback" ,G_CALLBACK (start_playback), NULL);
	g_signal_connect (playerObject, "handle-pause-playback" ,G_CALLBACK (pause_playback), NULL);
	g_signal_connect (playerObject, "handle-seek-track" ,G_CALLBACK (seek_track), NULL);
	g_signal_connect (playerObject, "handle-resume-playback" ,G_CALLBACK (resume_playback), NULL);
	g_signal_connect (playerObject, "handle-share-tracks", G_CALLBACK(share_tracks), NULL);
	g_signal_connect (playerObject, "handle-set-repeat-state", G_CALLBACK(set_repeat_state), NULL);
	g_signal_connect (playerObject, "handle-set-shuffle-state", G_CALLBACK(set_shuffle_state), NULL);
	g_signal_connect (playerObject, "handle-buffer-track", G_CALLBACK(buffer_track), NULL);
}

static void audio_service_initialise_client_player_resources (TinwellPlyrData *clientAppData)
{
	DEBUG ("entered");

	if(NULL != clientAppData)
	{
		clientAppData->player = g_object_new(GRASSMOOR_TYPE_AV_PLAYER, NULL);

		register_player_callbacks(clientAppData);
	}
}

void audio_service_remove_player_app_from_hash(const gchar *appName)
{
	DEBUG ("entered");

	if(NULL != appName)
	{
		TinwellPlyrData *clientAppData = g_hash_table_lookup(audioServicePlyrHash, (gpointer) appName);

		if(NULL != clientAppData)
		{
			/* destroy the application specific book keeping params held
			 * by audio service*/
			destroy_plyr_app_entry( (gpointer)appName, clientAppData, NULL);

			/* destroy application entry in audio service hash */
			g_mutex_lock(&audioServicePlyrMutex);
			gboolean remove = g_hash_table_remove(audioServicePlyrHash, (gpointer)appName);
			DEBUG ("Removal status = %d", remove);
			g_mutex_unlock(&audioServicePlyrMutex);
		}
	}
}

static TinwellPlyrData* audio_service_create_plyr_client_struct(const gchar *appName)
{
	TinwellPlyrData *clientAppData = g_new0(TinwellPlyrData, 1);

	/* Assign the parameters that are to be treated as default */
	if(NULL != clientAppData)
	{
		clientAppData = g_new0(TinwellPlyrData, 1);

		clientAppData->name = g_strdup(appName);
		clientAppData->curPlayingTrack = NULL;
		clientAppData->playState = FALSE;
		clientAppData->repeat = FALSE;
		clientAppData->shuffle = FALSE;
		clientAppData->player = NULL;
		clientAppData->songsList = NULL;
		clientAppData->audioMgrPause = FALSE;
		clientAppData->clientPause = FALSE;
		/* FIXME: stub; assume music, because the one known client
		 * of Tinwell is Frampton, and that's what it plays. */
		clientAppData->audioResourceType = CANTERBURY_AUDIO_RESOURCE_MUSIC;
	}

	return clientAppData;
}

static void update_play_status (ClutterMedia *media ,GParamSpec *args1, TinwellPlyrData *plyrData)
{
	DEBUG ("entered");

	TinwellPlayer *playerObject = audio_service_player_object_get();

	if( (TINWELL_IS_PLAYER (playerObject)) && (NULL != plyrData) )
	{
		gboolean playState;
		gchar *uri = NULL;
		gdouble progress;

		/* Get the play-pause state, progress and uri of the media */
		g_object_get(media, "playing", &(playState), NULL);
		g_object_get(media, "uri", &uri, NULL);
		g_object_get(plyrData->player, "progress", &progress, NULL);

		plyrData->playState = playState;
		if(TRUE == playState)
		{
			if(NULL != plyrData->curPlayingTrack)
				g_free(plyrData->curPlayingTrack);

			plyrData->curPlayingTrack = g_strdup(uri);

			/* Set both audio manager and client pause status to true */
			plyrData->audioMgrPause = FALSE;
			plyrData->clientPause = FALSE;

			if(progress > 0)
			{
				/*Emit Play Resumed */
				tinwell_player_emit_playback_resumed(playerObject, plyrData->name, uri, progress);

				DEBUG ("Emit Resumed Message. App Name = %s, Track Name = %s Progress = %f",plyrData->name, uri, progress);
			}
			else
			{
				/* Emit Play Started */
				tinwell_player_emit_playback_started(playerObject, plyrData->name, uri);

				/* When playback starts, if the file is a part of an
				 * existing list of songs, keep track of the current
				 * index */
				plyrData->playIndex = g_list_index(plyrData->songsList, uri);

				DEBUG (" Emit Started Message. App Name = %s, Track Name = %s Progress = %f", plyrData->name, uri, progress);
			}
		}
		else
		{
			/* Emit Paused */
			tinwell_player_emit_playback_paused (playerObject, plyrData->name, uri, progress);

			DEBUG (" Emit Paused Message. App Name = %s, Track Name = %s Progress = %f", plyrData->name, uri, progress);
		}
	}
}

static void update_play_progress(ClutterMedia *media , GParamSpec *psepc, TinwellPlyrData *plyrData)
{
	DEBUG ("entered");

	TinwellPlayer *playerObject = audio_service_player_object_get();

	if( (TINWELL_IS_PLAYER (playerObject)) && (NULL != plyrData) )
	{
		/* Emit the signal only if the media is playing */
		if(TRUE == plyrData->playState)
		{
			gdouble progress;
			g_object_get(plyrData->player, "progress", &progress, NULL);

			/* Emit Paused */
			if(progress > 0)
			{
				tinwell_player_emit_playback_position (playerObject, plyrData->name, plyrData->curPlayingTrack, progress);

				DEBUG ("App Name = %s, Track Name = %s Progress = %f", plyrData->name, plyrData->curPlayingTrack, progress);
			}
		}
	}
}

static void trigger_playback (TinwellPlyrData *plyrData)
{
	if(NULL != plyrData)
	{
		appToPlay = plyrData->name;

		canterbury_audio_mgr_call_audio_request(audio_service_get_audio_mgr_proxy(), plyrData->name, NULL, NULL, NULL);
	}
}

static void notify_end_of_playback (ClutterMedia *media,  TinwellPlyrData *plyrData)
{
	CanterburyAudioMgr *proxyAudioMgr = audio_service_get_audio_mgr_proxy();

	if(NULL != plyrData)
	{
		/* Notify end of playback immediately */
		tinwell_player_emit_playback_completed (playerObject, plyrData->name, plyrData->curPlayingTrack);
		DEBUG ("App Name = %s, Track Name = %s", plyrData->name, plyrData->curPlayingTrack);

		if(CANTERBURY_IS_AUDIO_MGR (proxyAudioMgr))
		{
			gint index = -1;
			gchar *uri = NULL;

			if(CANTERBURY_AUDIO_RESOURCE_INTERRUPT == plyrData->audioResourceType)
				canterbury_audio_mgr_call_audio_release(proxyAudioMgr, plyrData->name, NULL, NULL, NULL);

			/* on end of playback, see if we have a continuous list to play
			 * */
			if(plyrData->playIndex >= 0)
			{
				/* Repeat track if repeat is enabled */
				if(TRUE == plyrData->repeat)
					index = plyrData->playIndex;
				else if (TRUE == plyrData->shuffle)
				{
					/* If shuffle is selected, select a new song for shuffle */
					gint32 shuffleIndex = g_rand_int_range(g_rand_new(), 0, g_list_length (plyrData->songsList));
					if(shuffleIndex != plyrData->playIndex)
						index = shuffleIndex;
					else
					{
						index = index + 1;
						if(index > g_list_length(plyrData->songsList))
							index = 0;
					}
				}
				else
					index =  index + 1;
			}
			/* Repeat feature maybe enabled for apps with single URL for
			 * play also*/
			else
			{
				if(TRUE == plyrData->repeat)
					uri = plyrData->curPlayingTrack;
			}


			/* If a list of tracks was earlier shared, calculate the index
			 * and set the file uri for playback */
			if(index >= 0)
				uri = g_list_nth_data(plyrData->songsList, index);

			if(NULL != uri)
			{
				DEBUG ("Play track from the list = %s",uri);
				g_object_set(plyrData->player, "uri", uri, NULL);
				trigger_playback(plyrData);
			}
		}
	}
}

static void indicate_media_error (ClutterMedia *media , GError *error, TinwellPlyrData *plyrData)
{
	TinwellPlayer *playerObject = audio_service_player_object_get();

	if( (NULL != error) && (TINWELL_IS_PLAYER (playerObject)) && (NULL != plyrData) )
	{
		tinwell_player_emit_playback_error (playerObject, plyrData->name, plyrData->curPlayingTrack, error->message);

		DEBUG ("App Name = %s, Track Name = %s, Error msg = %s", plyrData->name, plyrData->curPlayingTrack, error->message);
	}
}

static void update_buffering_percentage (ClutterMedia *media ,GParamSpec *args1, TinwellPlyrData *plyrData)
{
	TinwellPlayer *playerObject = audio_service_player_object_get();

	if( (TINWELL_IS_PLAYER (playerObject)) && (NULL != plyrData) )
	{
		gdouble buffered;
		g_object_get(plyrData->player, "buffer-fill", &buffered, NULL);

		tinwell_player_emit_buffer_position (playerObject, plyrData->name, plyrData->curPlayingTrack, buffered);

		DEBUG ("App Name = %s, Track Name = %s, Buffer percent = %f", plyrData->name, plyrData->curPlayingTrack, buffered);
	}
}

# if 0
static void update_download_buffering(ClutterActor *player,gdouble start, gdouble stop, TinwellPlyrData *plyrData)
{
	TinwellPlayer *playerObject = audio_service_player_object_get();

	if( (TINWELL_IS_PLAYER (playerObject)) && (NULL != plyrData) )
	{
		tinwell_player_emit_buffer_position (playerObject, plyrData->name, plyrData->curPlayingTrack, stop);

		DEBUG ("App Name = %s, Track Name = %s, Buffer percent = %f", plyrData->name, plyrData->curPlayingTrack, stop);
	}
}
# endif

static void audio_service_free_player_tracks_list (TinwellPlyrData *clientAppData)
{
	DEBUG ("entered");

	/*If the history already exists purge it completely */
	if(NULL != clientAppData->songsList)
	{
		GList *tempList = g_list_first(clientAppData->songsList);
		while(tempList)
		{
			if(NULL != tempList->data)
			{
				g_free(tempList->data);
				tempList->data = NULL;
			}
			clientAppData->songsList = g_list_delete_link(clientAppData->songsList, tempList);
			tempList = g_list_first(clientAppData->songsList);
		}
	}

	clientAppData->songsList = NULL;
}

static void register_player_callbacks(TinwellPlyrData *plyrData)
{
	DEBUG ("entered");

	if(NULL != plyrData)
	{
		g_signal_connect(plyrData->player, "notify::playing",G_CALLBACK(update_play_status), plyrData);
		g_signal_connect(plyrData->player, "notify::progress",G_CALLBACK(update_play_progress), plyrData);
		g_signal_connect(plyrData->player, "error",G_CALLBACK(indicate_media_error), plyrData);
		g_signal_connect(plyrData->player, "eos",G_CALLBACK(notify_end_of_playback), plyrData);
		g_signal_connect(plyrData->player, "notify::buffer-fill",G_CALLBACK(update_buffering_percentage), plyrData);
		//g_signal_connect(plyrData->player, "download-buffering",G_CALLBACK(update_download_buffering), plyrData);
	}
}

static gboolean start_playback ( TinwellPlayer *object, GDBusMethodInvocation *invocation, const gchar *arg_app_name,
		const gchar *arg_url_path, gint arg_index)
{
	if( (NULL != arg_app_name) && (NULL != arg_url_path) )
	{
		DEBUG ("Start playback request by %s Track = %s, index = %d", arg_app_name, arg_url_path, arg_index);

		TinwellPlyrData *plyrData = audio_service_get_plyr_client_struct(arg_app_name);
    if( ( NULL != plyrData ) &&
        ( ( plyrData->audioResourceType == CANTERBURY_AUDIO_RESOURCE_INTERRUPT ) ||
          ( plyrData->audioResourceType == CANTERBURY_AUDIO_RESOURCE_PHONE) )
         &&
        ( GRASSMOOR_IS_AV_PLAYER(plyrData->player) == FALSE ) )
    {
      audio_service_initialise_client_player_resources(plyrData);
    }

		if( (NULL != plyrData) && (GRASSMOOR_IS_AV_PLAYER(plyrData->player)) )
		{
			gchar *uri = NULL;

			/* If a file has been selected, directly make that the uri to play */
			if(NULL != arg_url_path)
				uri = (gchar *)arg_url_path;
			else
			{
				/* if an index in an already shared list is passed,
				 * select that for playback */
				if(arg_index >= 0)
					uri = g_list_nth_data(plyrData->songsList, arg_index);
			}

			/* If the uri has been finally decided and selected, make
			 * a request to the audio manager to play */
			if(NULL != uri)
			{
				plyrData->clientPause = FALSE;
				g_object_set(plyrData->player, "uri", uri, NULL);

				//canterbury_audio_mgr_call_audio_request(audio_service_get_audio_mgr_proxy(), arg_app_name, NULL, NULL, NULL);
				trigger_playback(plyrData);
			}
			else
			{
				DEBUG ("Error msg = No uri selected. Cannot Play!");

				g_dbus_method_invocation_return_dbus_error (invocation, "Tinwell.Player.Error.Failed", "No uri selected. Cannot Play!");
				return TRUE;
			}

		}
		else
		{
			DEBUG ("Error msg = Client not registered!");

			g_dbus_method_invocation_return_dbus_error (invocation, "Tinwell.Player.Error.Failed", "Client not registered");
			return TRUE;
		}
	}
	else
	{
		DEBUG ("Error msg = Invalid App Name!");

		g_dbus_method_invocation_return_dbus_error (invocation, "Tinwell.Player.Error.Failed", "Invalid App Name !");
		return TRUE;
	}

	tinwell_player_complete_start_playback(object, invocation);

	return TRUE;
}

static gboolean pause_playback ( TinwellPlayer *object, GDBusMethodInvocation *invocation, const gchar *arg_app_name)
{
	if( (NULL != arg_app_name) )
	{
		DEBUG ("Pause playback request by %s", arg_app_name);

		TinwellPlyrData *plyrData = audio_service_get_plyr_client_struct(arg_app_name);

		if( (NULL != plyrData) && (GRASSMOOR_IS_AV_PLAYER(plyrData->player)) )
		{
			DEBUG ("Pause playback for %s",arg_app_name);

			plyrData->clientPause = TRUE;

			g_object_set(plyrData->player, "playing", FALSE, NULL);

			if(CANTERBURY_AUDIO_RESOURCE_INTERRUPT == plyrData->audioResourceType)
			{
				/* Release audio resouorce on pause */
				CanterburyAudioMgr *proxyAudioMgr = audio_service_get_audio_mgr_proxy();
				if(NULL != proxyAudioMgr)
					canterbury_audio_mgr_call_audio_release(proxyAudioMgr, plyrData->name, NULL, NULL, NULL);
			}
		}
		else
		{
			DEBUG ("Error msg = Client not registered!");

			g_dbus_method_invocation_return_dbus_error (invocation, "Tinwell.Player.Error.Failed", "Client not registered");
			return TRUE;
		}
	}
	else
	{
		DEBUG ("Error msg = Invalid App Name!");

		g_dbus_method_invocation_return_dbus_error (invocation, "Tinwell.Player.Error.Failed", "Invalid App Name");
		return TRUE;
	}

	tinwell_player_complete_pause_playback(object, invocation);

	return TRUE;
}

static gboolean resume_playback ( TinwellPlayer *object, GDBusMethodInvocation *invocation, const gchar *arg_app_name)
{
	if( (NULL != arg_app_name) )
	{
		DEBUG ("Resume playback request by %s", arg_app_name);

		TinwellPlyrData *plyrData = audio_service_get_plyr_client_struct(arg_app_name);

		if( (NULL != plyrData) && (GRASSMOOR_IS_AV_PLAYER(plyrData->player)))
		{
			DEBUG ("Send request to audio manager for resume playback for %s", arg_app_name);

			plyrData->clientPause = FALSE;

		//	if (FALSE == plyrData->audioMgrPause)
			{
				//canterbury_audio_mgr_call_audio_request(audio_service_get_audio_mgr_proxy(), arg_app_name, NULL, NULL, NULL);
				trigger_playback(plyrData);
			}
		}
		else
		{
			DEBUG ("Error msg = Client not registered!");

			g_dbus_method_invocation_return_dbus_error (invocation, "Tinwell.Player.Error.Failed", "Client not registered");
			return TRUE;
		}
	}
	else
	{
		DEBUG (" %s: Error msg = %s", __FUNCTION__, "Invalid App Name!");

		g_dbus_method_invocation_return_dbus_error (invocation, "Tinwell.Player.Error.Failed", "Invalid App name");
		return TRUE;
	}

	tinwell_player_complete_resume_playback(object, invocation);

	return TRUE;
}

static gboolean seek_track ( TinwellPlayer *object, GDBusMethodInvocation *invocation, const gchar *arg_app_name, gdouble seekPos)
{
	gboolean canSeek;

	if( (NULL != arg_app_name) )
	{
		DEBUG ("Seek  request by %s. Set track pos to %f", arg_app_name, seekPos);

		TinwellPlyrData *plyrData = audio_service_get_plyr_client_struct(arg_app_name);

		if( (NULL != plyrData) && (GRASSMOOR_IS_AV_PLAYER(plyrData->player)) )
		{
			g_object_get(plyrData->player, "can-seek", &canSeek, NULL);

			if(TRUE == canSeek)
			{
				DEBUG ("The track can seek. will set progress to %f", seekPos);

				g_object_set(plyrData->player, "progress", seekPos, NULL);
			}
		}
		else
		{
			DEBUG ("Error msg = Client not registered!");

			g_dbus_method_invocation_return_dbus_error (invocation, "Tinwell.Player.Error.Failed", "Client not registered");
			return TRUE;
		}
	}
	else
	{
		DEBUG ("Error msg = Invalid App Name!");

		g_dbus_method_invocation_return_dbus_error (invocation, "Tinwell.Player.Error.Failed", "Invalid App Name");
		return TRUE;
	}

	tinwell_player_complete_seek_track(object, invocation, canSeek);
	return TRUE;
}

static gboolean buffer_track (TinwellPlayer *object, GDBusMethodInvocation *invocation, const gchar *arg_app_name, gint arg_buffer_size)
{
	gboolean canBuffer = FALSE;

	if( (NULL != arg_app_name) )
	{
		DEBUG ("Buffering request by %s. Set buffer size to %d", arg_app_name, arg_buffer_size);

		TinwellPlyrData *plyrData = audio_service_get_plyr_client_struct(arg_app_name);

		if( (NULL != plyrData) && (GRASSMOOR_IS_AV_PLAYER(plyrData->player)) )
		{
			g_object_get(plyrData->player, "can-buffer", &canBuffer, NULL);

			if(TRUE == canBuffer)
			{
				/* Buffer the track */
			}
		}
		else
		{
			DEBUG ("Error msg = Client not registered!");

			g_dbus_method_invocation_return_dbus_error (invocation, "Tinwell.Player.Error.Failed", "Client not registered");
			return TRUE;
		}
	}
	else
	{
		DEBUG ("Error msg = Invalid app name!");

		g_dbus_method_invocation_return_dbus_error (invocation, "Tinwell.Player.Error.Failed", "Invalid app name");
		return TRUE;
	}

	tinwell_player_complete_buffer_track(object, invocation, canBuffer);
	return TRUE;
}

static gboolean set_repeat_state (TinwellPlayer *object, GDBusMethodInvocation *invocation, const gchar *arg_app_name, gboolean repeatState)
{
	if( (NULL != arg_app_name) )
	{
		DEBUG ("Repeat request by %s. Set to %d state", arg_app_name, repeatState);

		TinwellPlyrData *plyrData = audio_service_get_plyr_client_struct(arg_app_name);

		if( (NULL != plyrData) && (GRASSMOOR_IS_AV_PLAYER(plyrData->player)) )
		{
			DEBUG ("Repeat state will be set");

			plyrData->repeat = repeatState;
		}
		else
		{
			DEBUG ("Error msg = Client not registered!");

			g_dbus_method_invocation_return_dbus_error (invocation, "Tinwell.Player.Error.Failed", "Client not registered");
			return TRUE;
		}
	}
	else
	{
		DEBUG ("Error msg = Invalid app name!");

		g_dbus_method_invocation_return_dbus_error (invocation, "Tinwell.Player.Error.Failed", "Invalid App Name");
		return TRUE;
	}

	tinwell_player_complete_set_repeat_state(object, invocation);
	return TRUE;
}

static gboolean set_shuffle_state (TinwellPlayer *object, GDBusMethodInvocation *invocation, const gchar *arg_app_name, gboolean shuffleState)
{
	if( (NULL != arg_app_name) )
	{
		DEBUG ("shuffle request by %s. Set to %d state", arg_app_name, shuffleState);

		TinwellPlyrData *plyrData = audio_service_get_plyr_client_struct(arg_app_name);

		if( (NULL != plyrData) && (GRASSMOOR_IS_AV_PLAYER(plyrData->player)) )
		{
			DEBUG ("Shuffle state will be set");

			plyrData->shuffle = shuffleState;
		}
		else
		{
			DEBUG ("Error msg = Client not registered!");

			g_dbus_method_invocation_return_dbus_error (invocation, "Tinwell.Player.Error.Failed", "Client not registered");
			return TRUE;
		}
	}
	else
	{
		DEBUG ("Error msg = Invalid app name!");

		g_dbus_method_invocation_return_dbus_error (invocation, "Tinwell.Player.Error.Failed", "Invalid app name");
		return TRUE;
	}

	tinwell_player_complete_set_shuffle_state(object, invocation);
	return TRUE;
}

static gboolean share_tracks (TinwellPlayer *object, GDBusMethodInvocation *invocation, const gchar *arg_app_name,
		const gchar *const *arg_tracks_list, gboolean new_list)
{
	if( (NULL != arg_app_name) )
	{
		DEBUG ("Share tracks request by %s", arg_app_name);

		TinwellPlyrData *plyrData = audio_service_get_plyr_client_struct(arg_app_name);
		if(NULL != plyrData)
		{
			gint counter = 0;

			if(TRUE == new_list)
			{
				DEBUG ("New tracks list recieved for %s", arg_app_name);

				audio_service_free_player_tracks_list(plyrData);
			}

			for(counter = 0; arg_tracks_list[counter] != NULL; counter++)
				plyrData->songsList = g_list_append(plyrData->songsList, g_strdup(((gchar *)arg_tracks_list[counter])));
		}
		else
		{
			DEBUG ("Error msg = Client not registered!");

			g_dbus_method_invocation_return_dbus_error (invocation, "Tinwell.Player.Error.Failed", "Client not registered");
			return TRUE;
		}
	}
	else
	{
		DEBUG ("Error msg = Invalid app name!");

		g_dbus_method_invocation_return_dbus_error (invocation, "Tinwell.Player.Error.Failed", "Invalid app name");
		return TRUE;
	}

	tinwell_player_complete_share_tracks(object, invocation);
	return TRUE;
}

static gboolean unregister_player(TinwellPlayer *object, GDBusMethodInvocation *invocation, const gchar *arg_app_name)
{
	if(NULL != arg_app_name)
	{
		DEBUG ("UnRegistration request by %s", arg_app_name);

		/* Fetch the app struct from the hash. If there is no
		 * entry in the hash, signal an error message */
		TinwellPlyrData *plyrData = audio_service_get_plyr_client_struct ( (gchar *)arg_app_name);

		if(NULL != plyrData)
		{
      /* call audio release for every unregistration if that app is currently holding the audio */
			gboolean playState = FALSE;
		  g_object_get(plyrData->player, "playing", &(playState), NULL);
		  CanterburyAudioMgr *proxyAudioMgr = audio_service_get_audio_mgr_proxy();
      if(playState)
	 	    canterbury_audio_mgr_call_audio_release(proxyAudioMgr, plyrData->name, NULL, NULL, NULL);

			/* Remove application entry from audio service hash tables
			 * */
			audio_service_remove_player_app_from_hash(arg_app_name);

			DEBUG ("Remove app %s from hash ", arg_app_name);

			/* Unregistration successful */
			tinwell_player_complete_unregister_player(object, invocation, TRUE);
		}
		else
		{
			DEBUG ("Error msg = Client not registered!");

			g_dbus_method_invocation_return_dbus_error (invocation, "Tinwell.Player.Error.Failed", "Client Not Registerd!");

			/* Unregistration not successful */
			tinwell_player_complete_unregister_player(object, invocation, FALSE);
		}
	}
	else
	{
		DEBUG ("Error msg = Invalid app name!");

		g_dbus_method_invocation_return_dbus_error (invocation, "Tinwell.Player.Error.Failed", "Invalid app name!");

		/* Unregistration not successful */
		tinwell_player_complete_unregister_player(object, invocation, FALSE);
	}
	return TRUE;
}

static gboolean register_player(TinwellPlayer *object, GDBusMethodInvocation *invocation, const gchar *arg_app_name)
{
	if(NULL != arg_app_name)
	{
		DEBUG ("Registration request by %s", arg_app_name);

		/* Check if the client is already registered. If so just return.
		 * If not, register the client */
		TinwellPlyrData *plyrData = audio_service_get_plyr_client_struct(arg_app_name);
		if(NULL != plyrData)
		{
			/* Already registered. No duplicate registrations */
			tinwell_player_complete_register_player(object, invocation, TRUE);
		}
		else
		{
			plyrData = audio_service_create_plyr_client_struct(arg_app_name);
			if(NULL != plyrData)
			{
				/* Add new client details to service hash list */
				audio_service_add_plyr_client_to_hash(arg_app_name, plyrData);

				/* Initialise player specific resources */
				audio_service_initialise_client_player_resources(plyrData);

				/* Registration Success */
				tinwell_player_complete_register_player(object, invocation, TRUE);
			}
			else
			{
				DEBUG ("Error msg = Cannot register client");

				g_dbus_method_invocation_return_dbus_error (invocation, "Tinwell.Player.Error.Failed", "Cannot register client");

				/* Registration Failure */
				tinwell_player_complete_register_player(object, invocation, FALSE);
			}

		}
	}
	else
	{
		DEBUG ("Error msg = Invalid app name");

		g_dbus_method_invocation_return_dbus_error (invocation, "Tinwell.Player.Error.Failed", "Invalid app name");

		/* Registration Failure */
		tinwell_player_complete_register_player(object, invocation, FALSE);
	}
	return TRUE;
}

TinwellPlyrData* audio_service_get_plyr_client_struct(const gchar *appName)
{
	DEBUG ("entered");
	TinwellPlyrData *clientAppData = NULL;


	if(NULL != audioServicePlyrHash)
	{
		/* Acquire the mutex lock before accessing the hash */
		g_mutex_lock(&audioServicePlyrMutex);

		/* Add the new entry into the hash table */
		clientAppData = g_hash_table_lookup(audioServicePlyrHash, (gpointer) appName);

		/* Release the mutex lock */
		g_mutex_unlock(&audioServicePlyrMutex);
	}

	return clientAppData;
}


void audio_service_initialise_player_resources(void)
{
	DEBUG ("entered");

	audio_service_plyr_mutex_init();

	if( NULL == audioServicePlyrHash )
		audio_service_plyr_hash_init();
}

void audio_service_plyr_resources_deinitialise(void)
{
	DEBUG ("entered");

	/* Deallocate all resources held for the hash table */
	audio_service_plyr_hash_deinit();

	/* Deallocate the resources needed for the mutex */
	audio_service_plyr_mutex_deinit();

	appToPlay = NULL;
}

gchar* audio_service_get_app_to_play(void)
{
	return appToPlay;
}

void audio_service_bus_acquired (GDBusConnection *connection, const gchar *name, gpointer user_data)
{
	DEBUG ("entered");

	/* Creates a new skeleton object, ready to be exported */
	playerObject = tinwell_player_skeleton_new();

	if(NULL != playerObject)
	{
		/* provide signal handlers for all methods */
		register_player_handlers(playerObject);

		/* Exports interface playerObject at object_path "/Tinwell/Player" on connection */
		if (!g_dbus_interface_skeleton_export (G_DBUS_INTERFACE_SKELETON (playerObject), connection, "/org/apertis/Tinwell/Player", NULL))
			CRITICAL ("player interface skeleton export error");
	}

	/* create interface for audio recorder */
	audio_service_recorder_interface_initialize(connection);
}

void audio_service_name_acquired (GDBusConnection *connection, const gchar *name, gpointer user_data)
{
	DEBUG ("%s", name);
}

void audio_service_name_lost (GDBusConnection *connection, const gchar *name, gpointer user_data)
{
	DEBUG ("%s ", name);
	/* free all dbus handlers ... */
}

TinwellPlayer* audio_service_player_object_get(void)
{
	return playerObject;
}

