tinwell (0.2020.1) apertis; urgency=medium

  * Switch to native format to work with the GitLab-to-OBS pipeline.
  * gitlab-ci: Link to the Apertis GitLab CI pipeline definition.

 -- Andrew Lee (李健秋) <andrew.lee@collabora.co.uk>  Tue, 21 Jan 2020 18:20:59 +0800

tinwell (0.1706.0-0co6) apertis; urgency=medium

  * debian/control: Depend on the hotdoc-0.8 branch

 -- Emanuele Aina <emanuele.aina@collabora.com>  Wed, 08 Jan 2020 16:02:24 +0000

tinwell (0.1706.0-0co5) apertis; urgency=medium

  * debian/control: add dh-apparmor as build-deps.
  * debian/rules: call dh_apparmor for each profile that we ship in
    binary packages.

 -- Andrew Lee (李健秋) <andrew.lee@collabora.co.uk>  Fri, 01 Nov 2019 17:02:57 +0800

tinwell (0.1706.0-0co4) apertis; urgency=medium

  * d/changelog: Fix syntax error in changelog file. The syntax error was
    generating broken build files 

 -- Ritesh Raj Sarraf <ritesh.sarraf@collabora.com>  Fri, 05 Jul 2019 17:38:53 +0530

tinwell (0.1706.0-0co3) apertis; urgency=medium

  * Re-enable the tinwell-doc package 

 -- Ritesh Raj Sarraf <ritesh.sarraf@collabora.com>  Fri, 19 Apr 2019 07:11:29 +0000

tinwell (0.1706.0-0co2) apertis; urgency=medium

  * Re-enable hotdoc. 

 -- Ritesh Raj Sarraf <ritesh.sarraf@collabora.com>  Sat, 30 Mar 2019 04:37:55 +0000

tinwell (0.1706.0-0co1~1) apertis; urgency=medium

  * Disable hotdoc.

 -- Andrej Shadura <andrew.shadura@collabora.co.uk>  Fri, 15 Mar 2019 10:15:50 +0100

tinwell (0.1706.0-0co1) 17.06; urgency=medium

  * AppArmor: Allow D-Bus communication with Canterbury
  * AppArmor: Allow logging to the Journal
  * AppArmor: Allow Tinwell to communicate with any app-bundle
  * Stop using canterbury-dev transitional package

 -- Simon McVittie <smcv@collabora.com>  Fri, 21 Apr 2017 18:28:40 +0100

tinwell (0.1703.0-0co1) 17.03; urgency=medium

  [ Mathieu Duponchelle ]
  * Require hotdoc version 0.8
  * docs/reference/Makefile.am: use --gdbus-codegen-sources.

  [ Simon McVittie ]
  * Add debian/source/apertis-component marking this as a target package
  * Remove references to AppManager methods that are going away
    (part of T3448)

 -- Simon McVittie <simon.mcvittie@collabora.co.uk>  Wed, 01 Feb 2017 19:59:08 +0000

tinwell (0.2.3-0co1) 16.09; urgency=medium

  [ Mathieu Duponchelle ]
  * Documentation: stop using gtk-doc
  * debian: Remove doc-base file.
  * git.mk: update to upstream master
  * git.mk: Ignore files generated by hotdoc
  * documentation: port to hotdoc 0.8
  * configure.ac: enable silent rules

  [ Nandini Raju ]
  * Added glib logging mechanism

 -- Héctor Orón Martínez <hector.oron@collabora.co.uk>  Thu, 15 Sep 2016 23:20:21 +0200

tinwell (0.2.2-0co3) 16.09; urgency=medium

  * Fix suite: 0.2.2-0co2 never reached 16.06.

 -- Simon McVittie <simon.mcvittie@collabora.co.uk>  Fri, 22 Jul 2016 16:07:59 +0100

tinwell (0.2.2-0co2) 16.09; urgency=medium

  * Fix descriptions.
  * Make binary packages policy compliant. (Apertis: T1672)
  * Run wrap-and-sort -abst.

 -- Andrew Lee (李健秋) <ajqlee@debian.org>  Sat, 21 May 2016 20:49:32 +0800

tinwell (0.2.2-0co1) 16.03; urgency=medium

  [ abhiruchi ]
  * Added glib logging mechanism
  * Returning from the method handlers after dbus method invocation error.
  * Replaced exit with g_critical on canterbury proxy unavailability.

  [ Aleksander Morgado ]
  * Implemented introspection support for the gdbus client library
  * Use AX_COMPILER_FLAGS in build
  * build: rebuild gtk-doc sections always

  [ Sjoerd Simons ]
  * Drop left-over enums file

 -- Sjoerd Simons <sjoerd.simons@collabora.co.uk>  Wed, 02 Mar 2016 14:09:36 +0100

tinwell (0.2.1-0co1) 16.03; urgency=medium

  [ Philip Withnall ]
  * build: Add .arcconfig file

  [ Simon McVittie ]
  * .arcconfig: add default reviewers
  * Clean up build system to build with build-snapshot (T770):
    - don't define macros that aren't used with values that aren't defined
    - remove pointless indirection
    - make out-of-tree build work
    - Link to a library in the builddir, not the srcdir
    - Use usual name for systemduserunitdir, and put it in the ${prefix}
    - debian/source/options: ChangeLog is a generated file, ignore it
    - debian/rules: use autogen.sh so we can build directly from git
    - Use a static README file and the standard Autotools INSTALL
  * Fail the build if anything except *.la is built but not installed
  * Remove undesired dh_shlibdeps override
  * Replace entry-point schema with a desktop entry file (T574)

 -- Simon McVittie <simon.mcvittie@collabora.co.uk>  Fri, 08 Jan 2016 13:12:30 +0000

tinwell (0.2.0-0rb2co1) 15.09; urgency=medium

  * debian/source/options: ignore git.mk and .arcconfig in the Debian
    diff, since these are not intended to appear in tarballs but may
    appear in git
  * debian/usr.bin.tinwell: include <abstractions/chaiwala-base> so normal,
    safe functionality like loading shared libraries does not generate
    complaints (partial fix for Apertis#168)
  * debian/postinst: add #DEBHELPER# token so debhelper can generate code
  * debian/usr.bin.tinwell: allow connecting to the session bus
    and owning our bus name
  * debian/usr.bin.tinwell: allow reading the global GStreamer plugin
    registry cache, but audit writes, which are not necessarily appropriate

 -- Simon McVittie <simon.mcvittie@collabora.co.uk>  Mon, 24 Aug 2015 15:42:29 +0100

tinwell (0.2.0-0rb2) 15.03; urgency=low

  * Initial import

 -- Apertis package maintainers <packagers@lists.apertis.org>  Thu, 12 Mar 2015 16:24:40 +0530
