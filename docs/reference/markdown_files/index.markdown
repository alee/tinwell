# Tinwell

Tinwell is a service responsible for playback of audio streams and recording
of audio streams.

## Overview

### Tinwell Player Interface

The Tinwell player interface provides playback of audio streams. Player
interface provides various functionalities like playing a track, forwarding
to the next track, share tracks etc and signals to indicate different states.

Tinwell audio service depends on the Grassmoor AVPlayer and uses canterbury
audio manager service to request for audio resource for playback.

### Tinwell Recorder Interface

Tinwell recorder interface serves for recording of audio streams. Tinwell
Recorder interface depends on the libgrassmoor audio recorder for recording
interfaces. Recorder interface uses canterbury Audio Manager service to
request for audio resource for recording.

Recording is for now only be supported in speex format.

## Building

To build Tinwell from sources, get the latest source archives from
<https://git.apertis.org/cgit/tinwell.git/>. Once you have extracted
the sources from the archive execute the following commands in the
top-level directory:

```
$ ./autogen.sh
$ make
$ make install
```

You can configure the build with number of additional arguments passed
to the configure script, the full list of which can be obtained by
running `./configure --help`.

## Contact

[Mail the maintainers](mailto:maintainers@lists.apertis.org)
