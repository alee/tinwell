/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <gio/gio.h>
#include <tinwell.h>
#include <stdlib.h>
#include <stdio.h>
#include <glib.h>

static void name_vanished(GDBusConnection *connection, const gchar *name, gpointer user_data);
static void name_appeared (GDBusConnection *connection, const gchar *name, const gchar *name_owner, gpointer user_data);
static void tinwell_player_proxy_cb( GObject *source_object, GAsyncResult *res, gpointer user_data);
static void audio_client_unregister(void );
static void termination_handler (int signum);
static void register_exit_functionalities(void);
gpointer read_user_options(gpointer data);

static TinwellPlayer *proxyPlayer = NULL;
static gboolean playing = FALSE;
gboolean is_repeat;
gboolean is_shuffle;
int is_continous_play;

const gchar *client_name;
gchar *media_path;
GList *play_list=NULL;

GThread * Prt_read ;

static void name_vanished(GDBusConnection *connection, const gchar *name, gpointer user_data)
{
	g_print("\n Name Vanished");
	if(NULL != proxyPlayer)
		g_object_unref(proxyPlayer);
}

static void name_appeared (GDBusConnection *connection, const gchar *name, const gchar *name_owner, gpointer user_data)
{
	g_print("%s\n", __FUNCTION__);
	/* Asynchronously creates a proxy for the D-Bus interface */
	tinwell_player_proxy_new (connection,
			G_DBUS_PROXY_FLAGS_NONE,
			"org.apertis.Tinwell",
			"/org/apertis/Tinwell/Player",
			NULL,
			tinwell_player_proxy_cb,
			NULL);
}

static void unregistration_info_message_cb(GObject *source_object, GAsyncResult *res, gpointer user_data)
{
	gboolean regState;

	tinwell_player_call_unregister_player_finish(proxyPlayer, &regState, res , user_data);

	if(TRUE == regState)
		exit(1);
}

static void registration_info_message_cb(GObject *source_object, GAsyncResult *res, gpointer user_data)
{
	gboolean regState;

	tinwell_player_call_register_player_finish(proxyPlayer, &regState, res , user_data);

	/* If registration is successful, initiate a play on the given file */

	gchar * media_file= g_new0(gchar,100);
	gchar * media_list = g_new0(gchar,100);
	is_continous_play= FALSE;

	if(regState == TRUE)
	{
		fflush(stdin);
		setbuf(stdin,NULL);

		if(is_continous_play==FALSE)
		{
			g_print("\n Enter the Path to Play as [file:///path ..... (OR) http://link.....] : ");
			if(! scanf("%s",media_file))
				g_warning("no media file entered !!!!\n");
			play_list = g_list_append(play_list,media_file);
			is_continous_play=TRUE;
		}

		while(is_continous_play==TRUE)
		{
			setbuf(stdin,NULL);
			g_print("\n Enter the media files for continous play else enter (q) to play : ");
			if(! scanf("%s",media_list))
				g_warning("media files not provided !!!!\n");
			if (!g_strcmp0(media_list,"q"))
				break;

			else
			{
				play_list = g_list_append(play_list,g_strdup(media_list));
				/*  int i=0;
				    for(i=0;i < g_list_length (play_list);i++)
				    g_print("\n The link is %s %d",g_list_nth_data(play_list,i),i);*/

			}
			continue;
		}

		gint tmp;
		gchar ** pass_play_list=g_new0(gchar* , 100);
		for(tmp=0;tmp < g_list_length (play_list);tmp++)
		{
			pass_play_list[tmp] = g_strdup(g_list_nth_data(play_list,tmp));
			g_print("%s",pass_play_list[tmp]);
		}

		is_repeat = FALSE;
		is_shuffle=FALSE;

		g_print("\n 1. Press (1) to Pause Playing");
		g_print("\n 2. Press (2) to Resume Playing");
		g_print("\n 3. Press (3) to Enable Repeat Playing");
		g_print("\n 4. Press (4) to Shuffle Playing");
		g_print("\n 5. Press (5) to Disbale Repeat Playing");
		g_print("\n 6. Press (6) to Disable Shuffle Playing");
		g_print("\n \n !~~~~~~~~~~~~~~~~~~~~~~~~SAMPLE AUDIO CLIENT~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!");
		g_print("\n\033[s");

		if(g_list_length (play_list) == 1)
		{
			g_print("start playback\n");
			tinwell_player_call_start_playback(proxyPlayer,client_name, g_list_nth_data(play_list,0), -1, NULL, NULL, NULL);
		}
		else
		{
			tinwell_player_call_share_tracks (proxyPlayer,client_name,(const gchar * const*)pass_play_list,TRUE,NULL,NULL,NULL);
			tinwell_player_call_start_playback(proxyPlayer,client_name, NULL,1 , NULL, NULL, NULL);

		}

	}

	else
		exit(1);
}



static void play_completed_cb(TinwellPlayer *object, const gchar *arg_app_name, const gchar *arg_file_name)
{
	playing = FALSE;
	g_print("\033[u\033[4B");
	g_print("\n \033[K COMPLETED ");
	if((is_repeat == FALSE) && (is_shuffle ==FALSE) && (is_continous_play==TRUE))
		tinwell_player_call_unregister_player(proxyPlayer,client_name, NULL, unregistration_info_message_cb, NULL);
}

static void play_started_cb(TinwellPlayer *object, const gchar *arg_app_name, const gchar *arg_file_name)
{
	playing = TRUE;

	Prt_read = g_thread_new ("read_user_play",read_user_options,(gpointer)arg_app_name);
}

static void play_position_cb(TinwellPlayer *object, const gchar *arg_app_name, const gchar *arg_file_name, gdouble arg_track_pos)
{
	gdouble tmp ;
	g_print("\033[u");
	g_print("\n \033[K %s \n \n \033[K[|",arg_file_name);

	for(tmp=0.0;tmp<(arg_track_pos*100);tmp=(tmp+5))
		g_print("->");
	g_print("] %d",(int)(arg_track_pos*100));
	g_print("\n \n \033[K (>) PLAYING");
	g_print("\n \n \n !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!!!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!");
	g_print("\033[3A");
}

static void play_resumed_cb(TinwellPlayer *object, const gchar *arg_app_name, const gchar *arg_file_name, gdouble arg_track_pos)
{
	playing = TRUE;

	Prt_read = g_thread_new("user_option",read_user_options,(gpointer)arg_app_name);
}

static void play_paused_cb(TinwellPlayer *object, const gchar *arg_app_name, const gchar *arg_file_name, gdouble arg_track_pos)
{
	playing = FALSE;

	g_print("\033[u\033[4B");
	g_print("\n \033[K || PAUSED ");

	Prt_read = g_thread_new ("user_options_pause",read_user_options,(gpointer)arg_app_name);
}

static void playback_error_cb(TinwellPlayer *object, const gchar *arg_app_name, const gchar *arg_file_name, gdouble arg_track_pos)
{
	g_print("\033[u\033[4B");
	g_print("\n \033[KERROR PLAYING TRACK");

}

static void set_repeat_state_cb (GObject *source_object,GAsyncResult *res,gpointer user_data)
{
	is_repeat = TRUE;
	Prt_read = g_thread_new ("user_options_repeat",read_user_options,user_data);
}

static void set_shuffle_state_cb(GObject *source_object,GAsyncResult *res,gpointer user_data)
{

	is_shuffle =TRUE;
	Prt_read = g_thread_new ("user_options_shuffle",read_user_options,user_data);
}

static void tinwell_player_proxy_cb( GObject *source_object, GAsyncResult *res, gpointer user_data)
{
	g_print("%s\n", __FUNCTION__);
	GError *error;

	gchar * name;
	/* finishes the proxy creation and gets the proxy ptr */
	proxyPlayer = tinwell_player_proxy_new_finish(res , &error);

	if(proxyPlayer == NULL)
	{
	}
	else
	{
		g_signal_connect(proxyPlayer, "playback-started", G_CALLBACK(play_started_cb), NULL);
		g_signal_connect(proxyPlayer, "playback-completed", G_CALLBACK(play_completed_cb), NULL);
		g_signal_connect(proxyPlayer, "playback-paused", G_CALLBACK(play_paused_cb), NULL);
		g_signal_connect(proxyPlayer, "playback-resumed", G_CALLBACK(play_resumed_cb), NULL);
		g_signal_connect(proxyPlayer, "playback-position", G_CALLBACK(play_position_cb), NULL);
		g_signal_connect(proxyPlayer, "playback-error", G_CALLBACK(playback_error_cb), NULL);
		//register for exit functionalities
		register_exit_functionalities();
		name =g_new0(gchar,200);
		//	g_print("Enter the client name");
		//scanf("%s",name);
		client_name = "AudioPlayer";//name;

		tinwell_player_call_register_player(proxyPlayer, client_name, NULL, registration_info_message_cb, NULL);
	}
}

gpointer read_user_options(gpointer data)
{

	gint user_option;
	fflush(stdin);
	setbuf(stdin,NULL);

	if(! scanf(" %d",&user_option))
	{
		g_warning("no input provided");
		return FALSE;
	}
	switch(user_option)
	{

		case 1:
			// g_print("\nInside Pause");
			tinwell_player_call_pause_playback(proxyPlayer, client_name, NULL, NULL, NULL);
			break;


		case 2:
			//  g_print("\n Inside resume");
			tinwell_player_call_resume_playback(proxyPlayer, client_name, NULL, NULL, NULL);
			break;

		case 3:
			is_repeat =TRUE;
			g_print("\033[u\033[6B");
			g_print ("\n REPEAT : ON ");
			tinwell_player_call_set_repeat_state (proxyPlayer,client_name,TRUE,NULL,set_repeat_state_cb,NULL);
			break;

		case 4:
			is_shuffle =TRUE;
			g_print("\033[u\033[6B");
			g_print("\n \t \t \t \t SHUFFLE : ON ");
			tinwell_player_call_set_repeat_state (proxyPlayer,client_name,TRUE,NULL,set_shuffle_state_cb,NULL);
			break;

		case 5:
			is_repeat =FALSE;
			g_print("\033[u\033[6B");
			g_print ("\n REPEAT : OFF");
			tinwell_player_call_set_repeat_state (proxyPlayer,client_name,FALSE,NULL,set_repeat_state_cb,NULL);
			break;

		case 6:
			is_shuffle =FALSE;
			g_print("\033[u\033[6B");
			g_print("\n \t \t \t \t SHUFFLE : OFF");

			tinwell_player_call_set_repeat_state (proxyPlayer,client_name,FALSE,NULL,set_shuffle_state_cb,NULL);
			break;
	}
	g_thread_exit(NULL);
	return FALSE;
}

static void termination_handler (int signum)
{
	exit(0);
}

static void register_exit_functionalities(void)
{
	struct sigaction action;
	action.sa_handler = termination_handler;
	action.sa_flags = 0;
	sigaction(SIGINT, &action, NULL);
	sigaction(SIGHUP, &action, NULL);
	sigaction(SIGTERM, &action, NULL);
	atexit(audio_client_unregister);
}

static void audio_client_unregister(void )
{
	tinwell_player_call_unregister_player(proxyPlayer,client_name, NULL, unregistration_info_message_cb, NULL);
}

gint main(gint argc, gchar *argv[])
{

	GMainLoop *mainloop = NULL;
//	g_type_init();



	gint watch;

	/* Starts watching name on the bus specified by bus_type */
	/* calls name_appeared_handler and name_vanished_handler when the name is known to have a owner or known to lose its owner */
	watch = g_bus_watch_name (G_BUS_TYPE_SESSION,
			"org.apertis.Tinwell",
			G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
			name_appeared,
			name_vanished,
			NULL,
			NULL);

	mainloop = g_main_loop_new (NULL, FALSE);
	g_main_loop_run (mainloop);

	exit (0);
}

